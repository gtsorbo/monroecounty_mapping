/*
 * MapTest.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package mapviewer;

public class MapTest {

	public static void main(String[] args) {
		MapFrame test = new MapFrame("monroe-county.tab");
		test.setVisible(true);
	}
}
