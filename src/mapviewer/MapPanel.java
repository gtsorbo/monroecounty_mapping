/*
 * MapPanel.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package mapviewer;

import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JPanel;

import graph.Edge;
import graph.Graph;

public class MapPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Graph mapData;
	Edge[] edges;
	boolean mwst;
	ArrayList<Edge> currentDirections;
	
	public MapPanel(String filename) {
		try {
			mapData = Graph.createFromFile(filename);
		} catch (IOException e) {
			System.out.println("~File not found~");
			e.printStackTrace();
		}
		mapData.generateMWST();
		edges = mapData.edgeHash.values().toArray(new Edge[mapData.edgeHash.size()]);
		mwst = false;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		setBackground(Color.BLACK);
		paintMapRoads(g);
		if(mwst) {
			paintMWST(g);
		}
		if(currentDirections != null) {
			paintDirections(g);
		}
	}
	
	private void paintMapRoads(Graphics g) {
		g.setColor(Color.cyan);
		for(Edge e : edges) {
			paintEdge(g, e);
		}
	}
	
	private void paintMWST(Graphics g) {
		g.setColor(Color.red);
		for(Edge e : mapData.mwst) {
			paintEdge(g, e);
		}
	}
	
	private void paintDirections(Graphics g) {
		g.setColor(Color.yellow);
		for(Edge e : currentDirections) {
			if(e != null) {
				paintEdge(g, e);
			}
		}
	}
	
	private void paintEdge(Graphics g, Edge e) {
		int maxX = (int)mapData.maxX;
		int maxY = (int)mapData.maxY;
		int height = getHeight();
		int width = getWidth();
		
		int x1 = (int)(width * (((e.v1.x)) / maxX));
		int y1 = (int)((height * ((e.v1.y) / maxY)));
		int x2 = (int)(width * ((e.v2.x) / maxX));
		int y2 = (int)((height * ((e.v2.y) / maxY)));
		
		g.drawLine(x1, y1, x2, y2);
	}
	
	public void setDirections(String vertex1, String vertex2) {
		currentDirections = mapData.dijkstraEdgePath(vertex1, vertex2);
	}
	
	public void toggleMWST() {
		mwst = !mwst;
	}
}
