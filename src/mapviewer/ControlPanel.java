/*
 * ControlPanel.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package mapviewer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ControlPanel extends JPanel implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextField inter1;
	private JTextField inter2;
	private JButton findPath;
	private JButton MWST;
	
	private MapPanel mapPanel;
	
	public ControlPanel(MapPanel mapPanel) {
		this.mapPanel = mapPanel;
		
		setLayout(new FlowLayout());
		inter1 = new JTextField("Intersection 1 ID");
		inter2 = new JTextField("Intersection 2 ID");
		findPath = new JButton("Find Path");
		MWST = new JButton("Show MWST");

		findPath.addActionListener(this);
		MWST.addActionListener(this);
		add(inter1);
		add(inter2);
		add(findPath);
		add(MWST);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == findPath) {
			mapPanel.setDirections(inter1.getText(), inter2.getText());
			mapPanel.repaint();
		}
		else if(e.getSource() == MWST) {
			mapPanel.toggleMWST();
			mapPanel.repaint();
			if(mapPanel.mwst) {
				MWST.setText("Hide MWST");
			}
			else if(!mapPanel.mwst) {
				MWST.setText("Show MWST");
			}
		}
		
	}
}
