/*
 * MapFrame.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package mapviewer;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class MapFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	MapPanel mapPanel;
	ControlPanel controlPanel;
	
	public MapFrame(String filename) {
		//super();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		mapPanel = new MapPanel(filename);
		controlPanel = new ControlPanel(mapPanel);
		setSize((int)(500*mapPanel.mapData.getRatio()), 500);
		setResizable(false);
		add(mapPanel, BorderLayout.CENTER);
		add(controlPanel, BorderLayout.SOUTH);
	}
}