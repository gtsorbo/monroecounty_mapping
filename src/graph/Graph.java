/*
 * Graph.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package graph;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.PriorityQueue;

/*
 * Graph.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: LAB 20
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: November 22, 2015
 */

public class Graph {
	public HashMap<String, Vertex> vertexHash;
	public HashMap<String, Edge> edgeHash;
	boolean directed;
	
	public double maxX;
	public double maxY;
	
	public ArrayList<Edge> mwst;
	
	public Graph() {
		vertexHash = new HashMap<String, Vertex>();
		edgeHash = new HashMap<String, Edge>();
		
		maxX = 0.0;
		maxY = 0.0;
	}
	
	public boolean isDirected() {
		return directed;
	}
	
	public void insertEdge(Edge e) {
		edgeHash.put(e.name, e);
	}
	
	public void insertVertex(Vertex v) {
		vertexHash.put(v.name, v);
	}
	
	public void deleteEdge(Edge e) {
		edgeHash.remove(e.name);
	}
	
	public void deleteVertex(Vertex v) {
		vertexHash.remove(v.name);
	}
	
	public double getRatio() {
		return maxX / maxY;
	}
	
	public static Graph createFromFile(String filename) throws IOException {
		BufferedReader fileReader = new BufferedReader(new FileReader(filename));
		Graph graph = new Graph();
		String line = "";
	
		while((line = fileReader.readLine()) != null) {
			String[] lineData = line.split("\\s+");
			if(lineData[0].equals("i")) {
				//keep track of max X and Y for display purposes
				double x = Double.parseDouble(lineData[2]);
				double y = Double.parseDouble(lineData[3]);
				if(x > graph.maxX) {
					graph.maxX = x;
				}
				if(y > graph.maxY) {
					graph.maxY = y;
				}
				// create Vertex object and add to graph
				Vertex newVertex = new Vertex(lineData[1], x, y);
				graph.insertVertex(newVertex);
			}
			else if(lineData[0].equals("r")) {
				// two edges, one in each "orientation"
				// only one gets added to graph, but need both for each Vertex's adjacency array
				Vertex v1 = graph.vertexHash.get(lineData[2]);
				Vertex v2 = graph.vertexHash.get(lineData[3]);
				
				Edge newEdge1 = new Edge(lineData[1], v1, v2);
				Edge newEdge2 = new Edge(lineData[1], v2, v1); // "reverse" edge
				graph.insertEdge(newEdge1);
				
				// Add edge to adjacency arrays in relevant Vertices
				graph.vertexHash.get(lineData[2]).adjacencies.add(newEdge1);
				graph.vertexHash.get(lineData[3]).adjacencies.add(newEdge2);
			}
		}
		
		fileReader.close();
		return graph;
	}

	public void dijkstra(String vertexID) {
		
		Vertex source = vertexHash.get(vertexID);
		
		for(Vertex v : vertexHash.values().toArray(new Vertex[vertexHash.size()])) {
			v.minDistance = Double.POSITIVE_INFINITY;
			v.known = false;
			v.previous = null;
		}
		
		source.minDistance = 0;
		
		PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
		vertexQueue.add(source);
		
		while(!vertexQueue.isEmpty()) {
			Vertex w = vertexQueue.poll();
			
			for(Edge edge : w.adjacencies) {
				Vertex v = edge.v2;
				double totalCost = w.minDistance + edge.weight;
				if(totalCost < v.minDistance) {
					vertexQueue.remove(v);
					
					v.minDistance = totalCost;
					v.previous = w;
					vertexQueue.add(v);
				}
			}
		}
	}
	
	public ArrayList<Vertex> dijkstraPath(String vertex1, String vertex2) {
		Vertex v1 = vertexHash.get(vertex1);
		Vertex v2 = vertexHash.get(vertex2);
		
		dijkstra(vertex1);
		ArrayList<Vertex> path = new ArrayList<Vertex>();
		
		while(!v2.equals(v1)) {
			path.add(v2);
			v2 = v2.previous;
		}
		path.add(v1);
		Collections.reverse(path);

		// Print out list of vertices for shortest path
		System.out.println("---Path as Vertices---");
		for(Vertex v : path) {
			System.out.println(v.name);
		}
		
		return path;
	}
	
	public ArrayList<Edge> dijkstraEdgePath(String vertex1, String vertex2) {
		ArrayList<Vertex> vertexPath = dijkstraPath(vertex1, vertex2);
		ArrayList<Edge> edgePath = new ArrayList<Edge>();
		for(int i=0; i<vertexPath.size(); i++) {
			Edge edgeToAdd = null;
			for(Edge e : vertexPath.get(i).adjacencies) {
				if(i+1 != vertexPath.size() && e.v2.equals(vertexPath.get(i+1))) {
					edgeToAdd = e;
					
				}
			}
			edgePath.add(edgeToAdd);
		}
		
		// Print out list of edges for shortest path
		System.out.println("---Path as Edges---");
		for(Edge e : edgePath) {
			if(e != null) {
				System.out.println(e.name);
			}
		}
		
		return edgePath;
	}
	
	public void generateMWST() {
		mwst = new ArrayList<Edge>();
		int vertexHashSize = vertexHash.size();
		
		ArrayList<Edge> edges = new ArrayList<Edge>(edgeHash.values());
		PriorityQueue<Edge> edgeHeap = new PriorityQueue<>(edges);
		DisjointSet ds = new DisjointSet(vertexHashSize);
				
		Vertex[] vertices = vertexHash.values().toArray(new Vertex[vertexHash.size()]);
		for(int i=0; i<vertices.length; i++) {
			vertices[i].mwstID = i;
		}
		
		while(mwst.size() != (vertexHashSize-1)) {
			Edge e = edgeHeap.poll();
			if(e != null) {
				int setU = ds.find(e.v1.mwstID);
				int setV = ds.find(e.v2.mwstID);
				if(setU != setV) {
					mwst.add(e);
					ds.union(setU, setV);
				}
			}
			else break;
		}
	}
}
