/*
 * Vertex.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package graph;

import java.util.ArrayList;

public class Vertex implements Comparable<Vertex>{
	public String name;
	public double x, y;
	
	public double minDistance = Double.POSITIVE_INFINITY;
	boolean known;
	public Vertex previous;
	public ArrayList<Edge> adjacencies;
	
	public int mwstID;
	public boolean mwstFound;
	
	public Vertex(String name, double x, double y) {
		this.name = name;
		this.x = x;
		this.y = y;
		mwstFound = false;
		adjacencies = new ArrayList<Edge>();
		mwstID = -1;
	}


	@Override
	public int compareTo(Vertex v2) {
		return Double.compare(minDistance, v2.minDistance);
	}
}
