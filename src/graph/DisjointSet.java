/*
 * Disjointset.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package graph;

// Based on the Disjoint Set pseudocode in the Weiss textbook

public class DisjointSet {

	private int[] s;
	
	public DisjointSet(int totalElements) {
		s = new int[totalElements];
		for(int i=0; i<totalElements; i++) {
			s[i] = -1;
		}
	}
	
	public void union(int r1, int r2) {
		if(s[r2] < s[r1]) {
			s[r1] = r2;
		}
		else {
			if(s[r1] == s[r2]) {
				s[r1]--;
			}
			s[r2] = r1;
		}
	}
	
	public int find(int x) {
		if(s[x] < 0) {
			return x;
		}
		else {
			s[x] = find(s[x]);
			return s[x];
		}
	}
}
