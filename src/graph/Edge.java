/*
 * Edge.java
 *
 * Version 1.0
 *
 * Copyright Grant Sorbo
 *
 * Course: CSC 172 FAll 2015
 *
 * Assignment: PROJECT 04
 *
 * Author: Grant Sorbo
 *
 * Lab Session: Tuesday/Thursday 2:00PM-3:15PM
 *
 * Lab TA: Mamiko Nagasaka
 *
 * Last Revised: December 12, 2015
 */

package graph;

public class Edge implements Comparable<Edge>{
	
	public String name;
	public final Vertex v1, v2;
	public double weight;
	
	public Edge(String name, Vertex v1, Vertex v2) {
		this.name = name;
		this.v1 = v1;
		this.v2 = v2;
		double xComp = Math.pow((v2.x - v1.x), 2);
		double yComp = Math.pow((v2.y - v1.y), 2);
		weight = Math.sqrt(xComp + yComp);
	}

	@Override
	public int compareTo(Edge o) {
		return Double.compare(weight, o.weight);
	}
}
